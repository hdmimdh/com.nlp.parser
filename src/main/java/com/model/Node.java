package com.model;

public class Node {

    private String type;
    private String value;
    private double score;

    private Node(Builder builder) {
        setType(builder.type);
        setValue(builder.value);
        setScore(builder.score);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public static final class Builder {
        private String type;
        private String value;
        private double score;

        private Builder() {
        }

        public Builder withType(String val) {
            type = val;
            return this;
        }

        public Builder withValue(String val) {
            value = val;
            return this;
        }

        public Builder withScore(double val) {
            score = val;
            return this;
        }

        public Node build() {
            return new Node(this);
        }
    }
}

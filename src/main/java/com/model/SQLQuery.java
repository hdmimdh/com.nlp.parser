package com.model;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SQLQuery {

    private List<String> selects = new ArrayList<>();

    private List<String> froms = new ArrayList<>();

    private List<String> wheres = new ArrayList<>();

    public List<String> getSelects() {
        return selects;
    }

    public void setSelects(List<String> selects) {
        this.selects = selects;
    }

    public List<String> getFroms() {
        return froms;
    }

    public void setFroms(List<String> froms) {
        this.froms = froms;
    }

    public List<String> getWheres() {
        return wheres;
    }

    public void setWheres(List<String> wheres) {
        this.wheres = wheres;
    }

    public String toSql() {

        if (CollectionUtils.isEmpty(selects) || CollectionUtils.isEmpty(froms)) {
            return "Illegal Sql Query";
        }
        return sqlOfSelect() +
                sqlOfFrom() +
                sqlOfWhere();
    }

    private String sqlOfSelect() {
        return sqlOf(selects);
    }


    private String sqlOfFrom() {
        return sqlOf(froms);
    }

    private String sqlOf(List<String> list) {

        if (CollectionUtils.isEmpty(list)) {
            return StringUtils.EMPTY;
        }

        StringBuilder sb = new StringBuilder();
        for (String val : list) {
            if (sb.length() == 0) {
                sb.append(val);
            } else {
                sb.append(", ").append(val);
            }
        }
        return sb.toString();
    }

    private String sqlOfWhere() {

        if (CollectionUtils.isEmpty(wheres)) {
            return StringUtils.EMPTY;
        }

        StringBuilder sb = new StringBuilder();
        for (String val : wheres) {
            if (sb.length() == 0) {
                sb.append(val);
            } else {
                // currently only allow for "AND"
                // TODO: add "OR"
                sb.append(" AND ").append(val);
            }
        }
        return sb.toString();
    }
}

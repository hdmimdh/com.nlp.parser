package com.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Schema {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    //table name, column name, column type
    private Map<String, Map<String, String>> tables = new HashMap<>();

    //table name, column name, column values
    private Map<String, Map<String, Set<String>>> tableRows = new HashMap<>();

    //table name, primary key (set of column names)
    private Map<String, Set<String>> keys = new HashMap<>();

    // table1Name, table2Name
    private Map<String, Set<String>> connections = new HashMap<>();

    public Map<String, Map<String, String>> getTables() {
        return tables;
    }

    public void setTables(Map<String, Map<String, String>> tables) {
        this.tables = tables;
    }

    public Map<String, Map<String, Set<String>>> getTableRows() {
        return tableRows;
    }

    public void setTableRows(Map<String, Map<String, Set<String>>> tableRows) {
        this.tableRows = tableRows;
    }

    public Map<String, Set<String>> getKeys() {
        return keys;
    }

    public void setKeys(Map<String, Set<String>> keys) {
        this.keys = keys;
    }

    public Map<String, Set<String>> getConnections() {
        return connections;
    }

    public void setConnections(Map<String, Set<String>> connections) {
        this.connections = connections;
    }

    public String json() {

        try {
            return MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {

            System.out.println("Failed to convert schema graph to a valid JSON");
            return StringUtils.EMPTY;
        }
    }

}

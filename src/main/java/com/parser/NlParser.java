package com.parser;

import com.model.Schema;
import com.util.Unit;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.WordLemmaTag;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Natural language parser, a wrapper of the Stanford NLP parser.
 */
public class NlParser {

    private static final String NOUN_TAG = "NN";
    private static MaxentTagger tagger = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger");

    public List<WordLemmaTag> nlLemmas(String nlText) {

        if (StringUtils.isBlank(nlText)) {
            return Collections.emptyList();
        }

        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma, parse");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        Annotation document = new Annotation(nlText);
        pipeline.annotate(document);

        List<WordLemmaTag> lemmas = new ArrayList<>();

        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        if (CollectionUtils.isEmpty(sentences)) {
            return Collections.emptyList();
        }

        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {

                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String tag = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);

                lemmas.add(new WordLemmaTag(word, lemma, tag));
            }
        }
        return lemmas;
    }

    public List<Unit> nlTables(List<WordLemmaTag> lemmas, Schema schema) {

        if (CollectionUtils.isEmpty(lemmas) || schema == null) {
            return null;
        }

        lemmas = lemmas.stream()
                .filter(lemma -> StringUtils.contains(lemma.tag(), NOUN_TAG))
                .collect(Collectors.toList());

        List<Unit> tables = new ArrayList<>();
        for (WordLemmaTag lemma : lemmas) {

            for (String table : schema.getTables().keySet()) {

                Morphology morphology = new Morphology();
                WordLemmaTag tableLemma = morphology.lemmatize(new WordTag(table, tagger.tagString(table)));

                tables.add(
                        Unit.newBuilder()
                                .withName(table)
                                .withType(Unit.UnitType.TABLE)
                                .withTag(lemma.word())
                                .withScore(WordSimilarity.similarity(lemma.lemma(), tableLemma.lemma()))
                                .build());
            }
        }
        Collections.sort(tables);
        return tables;
    }
}

package com.parser;

import com.model.Schema;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SchemaParser {

    private static final String TABLE_ATTR = "TABLE";
    private static final String TABLE_NAME_ATTR = "TABLE_NAME";
    private static final String COLUMN_NAME_ATTR = "COLUMN_NAME";
    private static final String COLUMN_TYPE_NAME_ATTR = "TYPE_NAME";

    private static final Long LIMIT = 2000L;

    public Schema schema(Connection connection) throws SQLException {

        if (connection == null) {
            return null;
        }

        System.out.println("Retrieving schema graph...");
        DatabaseMetaData meta = connection.getMetaData();

        Schema schema = new Schema();

        String[] types = {TABLE_ATTR};
        ResultSet rsTable = meta.getTables(null, null, "%", types);
        while (rsTable.next()) {

            String tableName = rsTable.getString(TABLE_NAME_ATTR);

            schema.getTables().put(tableName, new HashMap<>());
            schema.getTableRows().put(tableName, new HashMap<>());

            Map<String, String> table = schema.getTables().get(tableName);
            Map<String, Set<String>> tableRow = schema.getTableRows().get(tableName);

            List<String> cols = new ArrayList<>();

            try (
                    ResultSet rsColumn = meta.getColumns(null, null, tableName, null)) {

                while (rsColumn.next()) {

                    String columnName = rsColumn.getString(COLUMN_NAME_ATTR);
                    String columnType = rsColumn.getString(COLUMN_TYPE_NAME_ATTR);

                    cols.add(columnName);
                    table.put(columnName, columnType);
                    tableRow.put(columnName, new HashSet<>());
                }
            }

            String query = "SELECT * FROM " + tableName + " ORDER BY RANDOM() LIMIT " + LIMIT + ";";

            try (
                    Statement stmt = connection.createStatement();
                    ResultSet rows = stmt.executeQuery(query)) {

                while (rows.next()) {

                    for (int i = 0; i < cols.size(); i++) {

                        String columnName = cols.get(i);
                        String columnValue = rows.getString(i + 1);

                        tableRow.get(columnName).add(columnValue);
                    }
                }
            }
        }

        loadKeys(schema, meta);
        loadTableConnections(schema);

        return schema;
    }

    private void loadKeys(Schema schema, DatabaseMetaData meta) throws SQLException {

        for (String tableName : schema.getTables().keySet()) {

            ResultSet rsPrimaryKey = meta.getPrimaryKeys(null, null, tableName);
            schema.getKeys().put(tableName, new HashSet<>());
            while (rsPrimaryKey.next()) {
                schema.getKeys().get(tableName).add(rsPrimaryKey.getString(COLUMN_NAME_ATTR));
            }
        }
    }

    private void loadTableConnections(Schema schema) {

        for (String tableName : schema.getTables().keySet()) {
            schema.getConnections().put(tableName, new HashSet<>());
        }

        for (String table1 : schema.getTables().keySet()) {
            for (String table2 : schema.getTables().keySet()) {

                if (table1.equals(table2)) {
                    continue;
                }
                if (!joinKeys(schema, table1, table2).isEmpty()) {

                    schema.getConnections().get(table1).add(table2);
                    schema.getConnections().get(table2).add(table1);
                }
            }
        }
    }

    private Set<String> joinKeys(Schema schema, String table1, String table2) {

        Set<String> table1Keys = schema.getKeys().get(table1);
        Set<String> table2Keys = schema.getKeys().get(table2);
        if (table1Keys.equals(table2Keys)) {
            return Collections.emptySet();
        }

        boolean keys1ContainedIn2 = true;
        for (String table1Key : table1Keys) {

            if (!schema.getTables().get(table2).containsKey(table1Key)) {
                keys1ContainedIn2 = false;
                break;
            }
        }
        if (keys1ContainedIn2) {
            return new HashSet<>(table1Keys);
        }

        boolean keys2ContainedIn1 = true;
        for (String table2Key : table2Keys) {

            if (!schema.getTables().get(table1).containsKey(table2Key)) {
                keys2ContainedIn1 = false;
                break;
            }
        }
        if (keys2ContainedIn1) {
            return new HashSet<>(table2Keys);
        }
        return Collections.emptySet();
    }

}

package com.parser;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class WordSimilarity {

    private static final ILexicalDatabase db = new NictWordNet();
    private static final RelatednessCalculator calculator = new WuPalmer(db);

    static {
        WS4JConfiguration.getInstance().setMFS(false);
    }

    public static double semantical(String word1, String word2) {
        return calculator.calcRelatednessOfWords(word1, word2);
    }

    public static double lexical(String word1, String word2) {

        Set<Character> charSet1 = new HashSet<>();
        Set<Character> charSet2 = new HashSet<>();
        Set<Character> commonSet = new HashSet<>();
        for (char c : word1.toCharArray()) {
            charSet1.add(c);
        }
        for (char c : word2.toCharArray()) {
            charSet2.add(c);
        }
        for (char c : charSet1) {
            if (charSet2.contains(c)) {
                commonSet.add(c);
            }
        }
        double jaccord = commonSet.size() / (double) (charSet1.size() + charSet2.size() + commonSet.size());
        return Math.sqrt(jaccord);
    }

    public static double similarity(String word1, String word2) {

        BigDecimal semantical = new BigDecimal(semantical(word1, word2));
        BigDecimal lexical = new BigDecimal(lexical(word1, word2));

        return semantical.multiply(lexical).doubleValue();
    }
}

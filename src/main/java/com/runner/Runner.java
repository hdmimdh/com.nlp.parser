package com.runner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Schema;
import com.parser.NlParser;
import com.parser.SchemaParser;
import com.util.Unit;
import edu.stanford.nlp.ling.WordLemmaTag;
import org.apache.commons.collections4.CollectionUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Runner {

    private static final String SQL_TEST_1 = "give me the number of customers who live in California";
    private static final String SQL_TEST_2 = "all persons in California";

    public static void main(String[] args) throws SQLException, IOException {

        NlParser nlParser = new NlParser();

        Schema schema = loadDBSchema();

        // parse NL text to lemmas
        List<WordLemmaTag> lemmas = nlParser.nlLemmas(SQL_TEST_1);

        // match noun lemmas to sql tables by similarity score
        List<Unit> nlTables = nlParser.nlTables(lemmas, schema);

        if (CollectionUtils.isEmpty(nlTables)) {
            return;
        }

        // best table match
        Unit highest = nlTables.get(0);

        // get cols of best table match
        Map<String, String> cols = schema.getTables().get(highest.getName());

    }

    private static Schema loadDBSchema() throws SQLException, IOException {

        Connection connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "postgres", "postgres");

        SchemaParser schemaParser = new SchemaParser();
        Schema schema = schemaParser.schema(connection);

        String json = schema.json();
        System.out.println(json);

        return new ObjectMapper().readValue(json, Schema.class);
    }
}

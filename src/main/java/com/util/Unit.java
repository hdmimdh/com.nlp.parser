package com.util;

public class Unit implements Comparable<Unit> {

    public enum UnitType {
        TABLE, COLUMN
    }

    private String tag;
    private String name;
    private UnitType type;
    private Double score;

    public Unit() {
    }

    private Unit(Builder builder) {
        setTag(builder.tag);
        setName(builder.name);
        setType(builder.type);
        setScore(builder.score);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnitType getType() {
        return type;
    }

    public void setType(UnitType type) {
        this.type = type;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public static final class Builder {
        private String name;
        private UnitType type;
        private Double score;
        private String tag;

        private Builder() {
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withType(UnitType val) {
            type = val;
            return this;
        }

        public Builder withScore(Double val) {
            score = val;
            return this;
        }

        public Unit build() {
            return new Unit(this);
        }

        public Builder withTag(String val) {
            tag = val;
            return this;
        }
    }

    @Override
    public int compareTo(Unit o) {
        return o.getScore().compareTo(this.score);
    }

    @Override
    public String toString() {
        return "Unit{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", score=" + score +
                '}';
    }
}

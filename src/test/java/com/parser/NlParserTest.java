package com.parser;

import edu.stanford.nlp.ling.WordLemmaTag;
import org.junit.Test;

import java.util.List;

public class NlParserTest {

    @Test
    public void nlLemmas() {

        NlParser nlParser = new NlParser();

        List<WordLemmaTag> lemmas = nlParser.nlLemmas("give me the number of customers who live in California");
        System.out.println(lemmas);

        lemmas = nlParser.nlLemmas("all persons in California");
        System.out.println(lemmas);;
    }
}
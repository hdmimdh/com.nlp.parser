package com.parser;

import org.junit.Test;

public class WordSimilarityTest {

    @Test
    public void compute() {

        System.out.println("\"California\", \"customer\"" + WordSimilarity.semantical("California", "customer"));
        System.out.println("\"person\", \"customer\"" + WordSimilarity.semantical("person", "customer"));
    }
}